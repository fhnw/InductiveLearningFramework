module ILF_Feature_Deduction_Passthrough {
	// Needs classes from this module
	requires ILF_Feature_Deduction_Interface;
	requires ILF_Data_Module;
	
	// Provides an implementation of this interface in the named class
	// Each module can only provide each service once! 
	// Alternate implementations must be in different modules!
	provides ilf.featureDeduction.ILF_featureDeduction with ilf.featureDeduction.auto.ILF_featureDeduction_auto;
}