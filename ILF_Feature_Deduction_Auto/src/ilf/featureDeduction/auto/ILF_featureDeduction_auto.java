package ilf.featureDeduction.auto;

import java.util.TreeSet;

import ilf.data.DataSet;
import ilf.data.FeatureType;
import ilf.featureDeduction.ILF_featureDeduction;

/**
 * The goal is to inspect each column of data, and deduce the data type best suited
 * to describe it. The primitive numeric types are relatively straightforward.
 * 
 * If the
 * data is not completely numeric, then we attempt to deduce an enumerated type.
 * If the total number of different values is less than the defined threshold (as
 * a percent of the total values), then the type is considered enumerated.
 *
 */
public class ILF_featureDeduction_auto implements ILF_featureDeduction {
	private static final int ENUM_THRESHOLD_PERCENT = 10; // TODO: should be a settable parameter
	
	private DataSet ds;

	@Override
	public String getName() {
		return "Auto";
	}

	@Override
	public boolean processDataSet(DataSet ds) {
		this.ds = ds;
		
		// Brute force...
		for (int col = 0; col < ds.getFeatureDataTypes().length; col++) {
			if (isInteger(col)) continue;
			if (isLong(col)) continue;
			if (isFloat(col)) continue;
			if (isDouble(col)) continue;
			if (isEnum(col)) continue;
			// Remains String
		}
		return true;
	}
	
	private boolean isDouble(int col) {
		boolean isDouble = true;
		Double minValue = Double.MAX_VALUE;
		Double maxValue = Double.MIN_VALUE;
		int numRecords = ds.getDataRecords().size();
		for (int i = 0; i < numRecords && isDouble; i++) {
			Object[] record = ds.getDataRecords().get(i);
			
			// Initially, everything is a String
			String value = (String) record[col];
			try {
				Double d = Double.parseDouble(value);
				if (d > maxValue) maxValue = d;
				if (d < minValue) minValue = d;
			} catch (NumberFormatException e) {
				isDouble = false;
			}
		}
		
		if (isDouble) {
			FeatureType t = new FeatureType(Double.class);
			t.setMaxValue(maxValue);
			t.setMinValue(minValue);
			ds.getFeatureDataTypes()[col] = t;
		}
		
		return isDouble;
	}
	
	private boolean isFloat(int col) {
		boolean isFloat = true;
		Float minValue = Float.MAX_VALUE;
		Float maxValue = Float.MIN_VALUE;
		int numRecords = ds.getDataRecords().size();
		for (int i = 0; i < numRecords && isFloat; i++) {
			Object[] record = ds.getDataRecords().get(i);
			
			// Initially, everything is a String
			String value = (String) record[col];
			try {
				Float d = Float.parseFloat(value);
				if (d > maxValue) maxValue = d;
				if (d < minValue) minValue = d;
			} catch (NumberFormatException e) {
				isFloat = false;
			}
		}
		
		if (isFloat) {
			FeatureType t = new FeatureType(Float.class);
			t.setMaxValue(maxValue);
			t.setMinValue(minValue);
			ds.getFeatureDataTypes()[col] = t;
		}
		
		return isFloat;
	}

	private boolean isLong(int col) {
		boolean isLong = true;
		Long minValue = Long.MAX_VALUE;
		Long maxValue = Long.MIN_VALUE;
		int numRecords = ds.getDataRecords().size();
		for (int i = 0; i < numRecords && isLong; i++) {
			Object[] record = ds.getDataRecords().get(i);
			
			// Initially, everything is a String
			String value = (String) record[col];
			try {
				Long d = Long.parseLong(value);
				if (d > maxValue) maxValue = d;
				if (d < minValue) minValue = d;
			} catch (NumberFormatException e) {
				isLong = false;
			}
		}
		
		if (isLong) {
			FeatureType t = new FeatureType(Long.class);
			t.setMaxValue(maxValue);
			t.setMinValue(minValue);
			ds.getFeatureDataTypes()[col] = t;
		}
		
		return isLong;
	}

	private boolean isInteger(int col) {
		boolean isInteger = true;
		Integer minValue = Integer.MAX_VALUE;
		Integer maxValue = Integer.MIN_VALUE;
		int numRecords = ds.getDataRecords().size();
		for (int i = 0; i < numRecords && isInteger; i++) {
			Object[] record = ds.getDataRecords().get(i);
			
			// Initially, everything is a String
			String value = (String) record[col];
			try {
				Integer d = Integer.parseInt(value);
				if (d > maxValue) maxValue = d;
				if (d < minValue) minValue = d;
			} catch (NumberFormatException e) {
				isInteger = false;
			}
		}
		
		if (isInteger) {
			FeatureType t = new FeatureType(Integer.class);
			t.setMaxValue(maxValue);
			t.setMinValue(minValue);
			ds.getFeatureDataTypes()[col] = t;
		}
		
		return isInteger;
	}
	
	private boolean isEnum(int col) {
		int numRecords = ds.getDataRecords().size();
		TreeSet<String> uniqueValues = new TreeSet<>();
		for (int i = 0; i < numRecords; i++) {
			Object[] record = ds.getDataRecords().get(i);
			uniqueValues.add((String) record[col]);
		}
		int numUniqueValues = uniqueValues.size();
		boolean isEnum = (numUniqueValues * 100 / numRecords <= ENUM_THRESHOLD_PERCENT);
		
		if (isEnum) {
			FeatureType t = new FeatureType(Enum.class);
			String[] enumValues = uniqueValues.toArray(new String[uniqueValues.size()]);
			t.setEnumValues(enumValues);
			ds.getFeatureDataTypes()[col] = t;
		}
		return isEnum;
	}
}
