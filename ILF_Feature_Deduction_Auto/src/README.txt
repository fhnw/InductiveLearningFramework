This module takes a parsed record where all fields are strings, and attempts
to automatically recognize the underlying data types of the attributes. For
example, if all values for an attribute can be correctly parsed as integers,
then the attribute is presumed to be an integer. The types recognized are:

- long: integer values
- double: decimal values
- date: YYYY-MM-DD format
- time: HH:mm:ss format
- timestamp: YYY-MM-DD HH:mm:ss format
- boolean: true/false

For all other values, the strings are retained, but reduced to a set, i.e., the
strings are essentially presumed to belong to an enumerated type. Obviously, in
some cases, the number of values may be the same as the number of data records.

The results are returned in a new Record object.