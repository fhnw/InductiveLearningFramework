package ilf.algorithm;

import java.util.ArrayList;
import java.util.List;
import java.util.ServiceLoader;

import ilf.data.DataSet;
import ilf.rules.RuleSet;

public interface ILF_algorithm {
	// The interface required for parsers
	public String getName(); // Each implementation has a name
	
	// Training interface
	public void setTrainingData(DataSet dataSet); // Provide data to algorithm
	public void Train(); // Train on the training data
	public void resetTraining(); // Delete training data and rules
	
	// Testing interface (after training)
	public void setTestingData(DataSet dataSet);
	public double Test(); // Test on the given data, return the accuracy rate (1.0 = perfect) 
	public RuleSet getRules(); // Get derived rules
	public void resetTesting(); // Delete testing data and results
	
	/**
	 * The static method used to discover available implementations on the module
	 * path
	 */
	static List<ILF_algorithm> getInstances() {
		ServiceLoader<ILF_algorithm> services = ServiceLoader.load(ILF_algorithm.class);
		List<ILF_algorithm> list = new ArrayList<>();
		services.iterator().forEachRemaining(list::add);
		return list;
	}
}
