module ILF_Algorithm_Interface {
	exports ilf.algorithm;
	
	// Requires data representation and rule representation classes
	requires transitive ILF_Data_Module;
	requires transitive ILF_Rule_Module;
	
	// Required for static getInstances method
	uses ilf.algorithm.ILF_algorithm;
}