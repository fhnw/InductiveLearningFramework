module ILF_Feature_Deduction_Interface {
	exports ilf.featureDeduction;
	
	// Requires data representation classes
	requires transitive ILF_Data_Module;
	
	// Required for static getInstances method
	uses ilf.featureDeduction.ILF_featureDeduction;
}