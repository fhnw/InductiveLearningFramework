package ilf.featureDeduction;

import java.util.ArrayList;
import java.util.List;
import java.util.ServiceLoader;

import ilf.data.DataSet;

public interface ILF_featureDeduction {
	// The interface required Parsers
	public String getName(); // Each implementation has a name
	public boolean processDataSet(DataSet in); // Examine data set, set feature types

	/**
	 * The static method used to discover available implementations on the module
	 * path
	 */
	static List<ILF_featureDeduction> getInstances() {
		ServiceLoader<ILF_featureDeduction> services = ServiceLoader.load(ILF_featureDeduction.class);
		List<ILF_featureDeduction> list = new ArrayList<>();
		services.iterator().forEachRemaining(list::add);
		return list;
	}
}
