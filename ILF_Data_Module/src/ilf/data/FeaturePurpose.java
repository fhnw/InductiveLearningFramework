package ilf.data;

public enum FeaturePurpose {
	INPUT, OUTPUT, IGNORE;
}
