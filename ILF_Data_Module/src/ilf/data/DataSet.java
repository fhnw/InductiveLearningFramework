package ilf.data;

import java.util.ArrayList;

public class DataSet {
	private String[] featureNames;
	private FeaturePurpose[] featurePurposes;
	private FeatureType[] featureDataTypes;
	
	private final ArrayList<Object[]> dataRecords;

	public DataSet() {
		dataRecords = new ArrayList<>();
	}
	
	public DataSet(String[] featureNames) {
		this();
		this.featureNames = featureNames;
	}
	
	public void addRecord(Object[] newRecord) {
		dataRecords.add(newRecord);
	}

	public String[] getFeatureNames() {
		return featureNames;
	}

	public void setFeatureNames(String[] featureNames) {
		this.featureNames = featureNames;
	}

	public FeaturePurpose[] getFeaturePurposes() {
		return featurePurposes;
	}

	public void setFeaturePurposes(FeaturePurpose[] featurePurposes) {
		this.featurePurposes = featurePurposes;
	}

	public FeatureType[] getFeatureDataTypes() {
		return featureDataTypes;
	}

	public void setFeatureDataTypes(FeatureType[] featureDataTypes) {
		this.featureDataTypes = featureDataTypes;
	}

	public ArrayList<Object[]> getDataRecords() {
		return dataRecords;
	}
}
