package ilf.data;

/**
 * A feature type describes the type of data stored in one attribute of a data
 * set.
 * 
 * - This may be a numeric type (such as integer), in which case we also record
 * the minimum and maximum observed values.
 * 
 * - It may also be an enumeration (a defined set of values, such as
 * "true"/"false"), in which case we record the observed values in an array.
 * 
 * If nothing more precise can be identified, the fallback type is "String",
 * which simply means that each value may be unique.
 */
public class FeatureType {
	private Class<?> type;
	private Number maxValue; // Only if numeric
	private Number minValue; // Only if numeric
	private String[] enumValues; // Only if numeric
	
	public static class FeatureTypeException extends RuntimeException {
		public FeatureTypeException(String str) {
			super(str);
		}
	}
	
	public FeatureType(Class<?> type) {
		if (type == String.class || Number.class.isAssignableFrom(type) || type == Enum.class) {
			this.type = type;
		} else {
			throw new FeatureTypeException("Unsupported feature type");
		}
	}
	
	@Override
	public String toString() {
		if (Number.class.isAssignableFrom(type)) {
			return type.getSimpleName() + "(" + minValue + ", " + maxValue + ")";
		} else if (type == Enum.class) {
			String out = "enum { ";
			for (String s : enumValues) out += s + " ";
			out += "}";
			return out;
		} else {
			return type.getSimpleName();
		}
	}
	
	public Class<?> getType() {
		return type;
	}

	public Number getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(Number maxValue) {
		this.maxValue = maxValue;
	}

	public Number getMinValue() {
		return minValue;
	}

	public void setMinValue(Number minValue) {
		this.minValue = minValue;
	}

	public String[] getEnumValues() {
		return enumValues;
	}

	public void setEnumValues(String[] enumValues) {
		this.enumValues = enumValues;
	}
}
