A data set is a complete set of data records. This class represents the contents of
a data set as generally as possible.