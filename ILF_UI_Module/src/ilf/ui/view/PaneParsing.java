package ilf.ui.view;

import java.util.List;

import ilf.parser.ILF_parser;
import ilf.ui.commonClasses.Translator;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class PaneParsing extends PaneModuleSelector {
	
	public PaneParsing() {
		super();
	}
	
	public void updateTexts(Translator t) {
		lblPaneTitle.setText(t.getString("program.parsing.title"));
	}
	
	@Override
	protected ObservableList<String> getAvailableModules() {
		ObservableList<String> moduleNames = FXCollections.observableArrayList();
		List<ILF_parser> parserModules = ILF_parser.getInstances();
		for (ILF_parser p : parserModules) moduleNames.add(p.getName());
		return moduleNames;
	}
}
