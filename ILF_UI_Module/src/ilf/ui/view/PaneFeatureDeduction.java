package ilf.ui.view;

import java.util.List;

import ilf.featureDeduction.ILF_featureDeduction;
import ilf.ui.commonClasses.Translator;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class PaneFeatureDeduction extends PaneModuleSelector {
	
	public PaneFeatureDeduction() {
		super();
	}
	
	public void updateTexts(Translator t) {
		lblPaneTitle.setText(t.getString("program.featureDeduction.title"));
	}
	
	@Override
	protected ObservableList<String> getAvailableModules() {
		ObservableList<String> moduleNames = FXCollections.observableArrayList();
		List<ILF_featureDeduction> parserModules = ILF_featureDeduction.getInstances();
		for (ILF_featureDeduction p : parserModules) moduleNames.add(p.getName());
		return moduleNames;
	}
}
