package ilf.ui.view.featureSelection;

import javafx.beans.property.SimpleStringProperty;
import ilf.data.FeaturePurpose;
import javafx.beans.property.SimpleObjectProperty;

public class FeatureRow {
	final SimpleStringProperty featureName;
	final SimpleObjectProperty<FeaturePurpose> featurePurpose;
	
	public FeatureRow(String featureName, FeaturePurpose featurePurpose) {
		this.featureName = new SimpleStringProperty(featureName);
		this.featurePurpose = new SimpleObjectProperty<>(featurePurpose);
	}
}
