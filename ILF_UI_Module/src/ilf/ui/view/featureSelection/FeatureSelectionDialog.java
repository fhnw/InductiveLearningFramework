package ilf.ui.view.featureSelection;

import ilf.data.DataSet;
import ilf.data.FeaturePurpose;
import ilf.ui.ServiceLocator;
import ilf.ui.commonClasses.Translator;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * This class displays a dialog showing a table of the feature purposes. The
 * user can alter the purposes of the features; when the dialog is closed with
 * the OK-button, the data-set is updated accordingly.
 * 
 * The user must select at least one INPUT feature and at least one OUTPUT
 * feature.
 */
public class FeatureSelectionDialog extends Dialog<Void> {
	Translator t = ServiceLocator.getServiceLocator().getTranslator();
	FeatureSelectionModel model;
	ButtonType btnOK;

	public FeatureSelectionDialog(Stage owner, DataSet ds) {
		super();

		// Create our own little MVC; this class is the view
		model = new FeatureSelectionModel(ds);
		createDialogView(owner, model);
	}

	private void createDialogView(Stage owner, FeatureSelectionModel model) {
		this.initOwner(owner);
		this.initStyle(StageStyle.UTILITY);
		this.setResizable(true);
		this.setTitle(t.getString("program.selectFeatures.title"));

		// Set up the table
		TableView<FeatureRow> table = new TableView<>();
		table.setEditable(true);
		table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

		TableColumn<FeatureRow, String> colName = new TableColumn<>(t.getString("program.selectFeatures.name"));
		colName.setCellValueFactory(c -> c.getValue().featureName);
		colName.setPrefWidth(250);
		table.getColumns().add(colName);

		TableColumn<FeatureRow, FeaturePurpose> colPurpose = new TableColumn<>(
				t.getString("program.selectFeatures.purpose"));
		colPurpose.setCellValueFactory(c -> c.getValue().featurePurpose);
		colPurpose.setCellFactory(ComboBoxTableCell.forTableColumn(FeaturePurpose.values()));
		colPurpose.setOnEditCommit(editEvent -> {
			editEvent.getRowValue().featurePurpose.set(editEvent.getNewValue());
			validate();
		});
		colPurpose.setMinWidth(130);
		colPurpose.setMaxWidth(130);
		table.getColumns().add(colPurpose);

		table.setItems(model.elements);
		getDialogPane().setContent(table);

		ButtonType btnCancel = new ButtonType(t.getString("program.selectFeatures.cancel"), ButtonData.CANCEL_CLOSE);
		getDialogPane().getButtonTypes().add(btnCancel);
		btnOK = new ButtonType(t.getString("program.selectFeatures.ok"), ButtonData.OK_DONE);
		getDialogPane().getButtonTypes().add(btnOK);
		validate();
	}

	private void validate() {
		getDialogPane().lookupButton(btnOK).setDisable(!model.isValid());
	}
}
