package ilf.ui.view.featureSelection;

import ilf.data.DataSet;
import ilf.data.FeaturePurpose;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class FeatureSelectionModel {
	final ObservableList<FeatureRow> elements = FXCollections.observableArrayList();

	/**
	 * The constructor creates the observable-list that is our table model,
	 * based on the information in the given dataset
	 */
	public FeatureSelectionModel(DataSet ds) {
		String[] featureNames = ds.getFeatureNames();
		FeaturePurpose[] featurePurposes = ds.getFeaturePurposes();		
		for (int i = 0; i < featureNames.length; i++) {
			FeatureRow row = new FeatureRow(featureNames[i], featurePurposes[i]);
			elements.add(row);
		}
	}
	
	/**
	 * Update the FeaturePurpose array from the table model
	 */
	public void setFeaturePurposes(DataSet ds) {
		FeaturePurpose[] featurePurposes = ds.getFeaturePurposes();		
		for (int i = 0; i < featurePurposes.length; i++) {
			FeatureRow row = elements.get(i);
			featurePurposes[i] = row.featurePurpose.get();
		}
	}
	
	/**
	 * Determine whether or not the feature-purposes are valid: at least one
	 * INPUT and one OUTPUT feature
	 */
	public boolean isValid() {
		boolean hasInput = false;
		boolean hasOutput = false;
		for (FeatureRow row : elements) {
			if (row.featurePurpose.get() == FeaturePurpose.INPUT) hasInput = true;
			if (row.featurePurpose.get() == FeaturePurpose.OUTPUT) hasOutput = true;
		}
		return hasInput && hasOutput;
	}
}
