package ilf.ui.view;

import javafx.beans.Observable;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

/**
 * This is the base class for the individual panes (process steps) in the GUI.
 * 
 * Every pane "disable" property inherited from Node. If this is set to true,
 * the pane must disable the appropriate controls to prevent user interaction.
 * By default this property is false, and no controls are enabled/disabled; this
 * should be overridden by individual panes as appropriate.
 * 
 * Each pane must also indicate its "ready" status - whether or not the user has
 * made all required selections. How this status is set, what validation takes
 * place, etc. - this is up to the individual pane. By default, a pane is always
 * ready; this may be appropriate for panes that begin with acceptable default
 * values, but should otherwise be overridden.
 * 
 * Finally, each pane has an "execution" status, stored in the StatusIndicator.
 * For most panes, the default status of "none" is correct; after execution of
 * the corresponding process step, this can be changed. The Status indicator is
 * normally integrated into the pane, so that it is visible to the user.
 */
public abstract class PaneBase extends HBox {
	protected final Label lblPaneTitle = new Label();
	protected final SimpleBooleanProperty ready = new SimpleBooleanProperty(true); // by default, we are ready
	public final StatusIndicator status = new StatusIndicator();

	public PaneBase() {
		super(10);
		this.getStyleClass().add("hbox");
		this.add(lblPaneTitle);

		// Initialize our disable property, and link disableControls to this method.
		// Arguably, this should be in a controller, but creating a controller for this
		// microscopic bit of code seems silly. The types in the lambda expression are
		// painful, but we would rather show them here than use a method reference and
		// inflict them on our subclasses.
		this.setDisable(false);
		this.disableProperty().addListener((ObservableValue<? extends Boolean> o, Boolean oldValue,
				Boolean newValue) -> disableControls(newValue));
	}

	protected void add(Node node) {
		this.getChildren().add(node);
	}

	protected void addAll(Node... nodes) {
		this.getChildren().addAll(nodes);
	}

	/**
	 * Method for enabling/disabling the controls in this pane.
	 */
	protected void disableControls(boolean disable) {
		// By default, do nothing
	}

	public boolean isReady() {
		return ready.get();
	}

	public SimpleBooleanProperty readyProperty() {
		return ready;
	}
}
