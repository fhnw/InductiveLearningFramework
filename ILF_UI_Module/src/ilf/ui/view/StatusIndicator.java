package ilf.ui.view;

import ilf.ui.ServiceLocator;
import ilf.ui.commonClasses.Translator;
import javafx.application.Platform;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.Label;

public class StatusIndicator extends Label {
	public static enum Status {
		NONE("lightgrey", 0), OK("lightgreen", 1), WARNING("lightyellow", 1), ERROR("lightpink", 1);

		private final String color;
		private final float opacity;
		
		private Status(String color, float opacity) {
			this.color = color;
			this.opacity = opacity;
		}
		
		@Override
		public String toString() {
			Translator t = ServiceLocator.getServiceLocator().getTranslator();
			return t.getString(this.name());
		}

		public String getColor() {
			return color;
		}

		public float getOpacity() {
			return opacity;
		}
	};

	private final SimpleObjectProperty<Status> statusProperty = new SimpleObjectProperty<>();

	public StatusIndicator() {
		super();
		this.setId("StatusLabel");
		this.statusProperty.setValue(Status.NONE);
	}

	public Status getStatus() {
		return statusProperty.getValue();
	}
	
	public SimpleObjectProperty<Status> getStatusProperty() {
		return statusProperty;
	}

	public void setStatus(Status status) {
		this.setStatus(status, "");
	}
	
	public void setStatus(Status status, String info) {
		this.statusProperty.setValue(status);

		Platform.runLater(() -> {
			this.setText(status.toString() + " " + info);
			this.setOpacity(status.getOpacity());
			this.setStyle("-fx-background-color: " + status.getColor());
		});
	}
}
