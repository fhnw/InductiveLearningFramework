package ilf.ui.view;

import java.util.List;

import ilf.algorithm.ILF_algorithm;
import ilf.ui.commonClasses.Translator;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class PaneAlgorithm extends PaneModuleSelector {
	
	public PaneAlgorithm() {
		super();
	}
	
	public void updateTexts(Translator t) {
		lblPaneTitle.setText(t.getString("program.algorithm.title"));
	}
	
	@Override
	protected ObservableList<String> getAvailableModules() {
		ObservableList<String> moduleNames = FXCollections.observableArrayList();
		List<ILF_algorithm> algorithmModules = ILF_algorithm.getInstances();
		for (ILF_algorithm a : algorithmModules) moduleNames.add(a.getName());
		return moduleNames;
	}
}
