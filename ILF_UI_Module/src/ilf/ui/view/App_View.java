package ilf.ui.view;

import java.util.Locale;
import java.util.logging.Logger;

import ilf.ui.ServiceLocator;
import ilf.ui.abstractClasses.View;
import ilf.ui.commonClasses.Translator;
import ilf.ui.model.App_Model;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Copyright 2018, FHNW, Prof. Dr. Brad Richards. All rights reserved. This code
 * is licensed under the terms of the BSD 3-Clause license (see the file
 * license.txt).
 * 
 * @author Brad Richards
 */
public class App_View extends View<App_Model> {
	private ServiceLocator sl;
	private Logger logger;
	private Translator t;

	// Menus
	private Menu menuFile;
	private Menu menuFileLanguage;
	private Menu menuHelp;
	public MenuItem mnuHelpAbout;

	// Components
	public PaneDataSelection paneDataSelection;
	public PaneParsing paneParsing;
	public PaneFeatureDeduction paneFeatureDeduction;
	public PaneFeatureSelection paneFeatureSelection;
	public PaneAlgorithm paneAlgorithm; // Rule abduction module
	public PaneTestParameters paneTestParameters;
	public PaneResults paneResults;

	// Other controls
	public Button btnLoadData;
	public Button btnTrain;

	public App_View(Stage stage, App_Model model) {
		super(stage, model);
		stage.getIcons().add(new Image(this.getClass().getResourceAsStream("ilf_icon.png")));
		updateTexts();
	}

	/**
	 * This is called from the super-class constructor, and hence executes before
	 * any local construction takes place
	 */
	@Override
	protected Scene create_GUI() {
		sl = ServiceLocator.getServiceLocator();
		logger = sl.getLogger();
		t = sl.getTranslator();

		VBox root = new VBox(10);
		root.getChildren().add(createMenus());
		root.getChildren().add(paneDataSelection = new PaneDataSelection());
		root.getChildren().add(paneParsing = new PaneParsing());
		root.getChildren().add(paneFeatureDeduction = new PaneFeatureDeduction());
		root.getChildren().add(new HBox(10, btnLoadData = new Button()));
		
		root.getChildren().add(paneFeatureSelection = new PaneFeatureSelection());
		root.getChildren().add(paneAlgorithm = new PaneAlgorithm());
		root.getChildren().add(paneTestParameters = new PaneTestParameters());
		root.getChildren().add(new HBox(10, btnTrain = new Button()));
		root.getChildren().add(paneResults = new PaneResults());
		
		Scene scene = new Scene(root);
		scene.getStylesheets().add(getClass().getResource("app.css").toExternalForm());
		logger.info("Application view initialized");
		return scene;
	}

	/**
	 * Create the menu bar. MenuItems for languages are handled here. Any other menu
	 * items should be handled in the controller
	 */
	private MenuBar createMenus() {
		MenuBar menuBar = new MenuBar();

		menuFile = new Menu();
		menuFileLanguage = new Menu();
		menuFile.getItems().add(menuFileLanguage);

		for (Locale locale : sl.getLocales()) {
			MenuItem language = new MenuItem(locale.getLanguage());
			menuFileLanguage.getItems().add(language);
			language.setOnAction(event -> {
				sl.getConfiguration().setLocalOption("Language", locale.getLanguage());
				sl.setTranslator(new Translator(locale.getLanguage()));
				updateTexts();
			});
		}

		mnuHelpAbout = new MenuItem();
		menuHelp = new Menu();
		menuHelp.getItems().add(mnuHelpAbout);
		
		menuBar.getMenus().addAll(menuFile, menuHelp);
		return menuBar;
	}

	protected void updateTexts() {
		stage.setTitle(t.getString("program.name"));
		
		Translator t = ServiceLocator.getServiceLocator().getTranslator();

		// The menu entries
		menuFile.setText(t.getString("program.menu.file"));
		menuFileLanguage.setText(t.getString("program.menu.file.language"));
		menuHelp.setText(t.getString("program.menu.help"));
		mnuHelpAbout.setText(t.getString("program.menu.help.about"));
		
		// Panes
		paneDataSelection.updateTexts(t);
		paneParsing.updateTexts(t);
		paneFeatureDeduction.updateTexts(t);
		paneFeatureSelection.updateTexts(t);
		paneAlgorithm.updateTexts(t);
		paneTestParameters.updateTexts(t);
		paneResults.updateTexts(t);

		// Other controls
		btnLoadData.setText(t.getString("program.execution.loadData"));
		btnTrain.setText(t.getString("program.execution.train"));
	}
}