package ilf.ui.view;

import ilf.ui.commonClasses.Translator;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;

public class PaneFeatureSelection extends PaneBase {
	public Button btnSelect = new Button();
	
	private Label lblSelectionStatus = new Label();
	
	public PaneFeatureSelection() {
		Region spacer = new Region();
		this.addAll(btnSelect, spacer, lblSelectionStatus);
		HBox.setHgrow(spacer, Priority.ALWAYS);
	}
	
	@Override
	protected void disableControls(boolean disable) {
		btnSelect.setDisable(disable);
	}
	
	public void updateTexts(Translator t) {
		lblPaneTitle.setText(t.getString("program.selectFeatures.title"));
		btnSelect.setText(t.getString("program.selectFeatures.select"));
	}
}
