package ilf.ui.view;

import ilf.rules.RuleSet;
import ilf.ui.commonClasses.Translator;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;

/**
 * The results pane does not derive from PaneBase, but rather directly
 * from VBox. It provides a brief overview of the learning results. For
 * more detail, the user must select the appropriate output option on
 * the TestParameters pane.
 * 
 * The results can be added one-by-one as the test runs are completed.
 */
public class PaneResults extends VBox {
	private final Label lblAverageAccuracy = new Label();
	private final Label lblBestAccuracy = new Label();
	private final TextArea txtRules = new TextArea();
	
	private double avgAccuracy;
	private double bestAccuracy;
	private double totalAccuracy;
	private int numTestRuns;
	private RuleSet bestRules;
	
	Translator t = null; // last translator used
	
	public PaneResults() {
		super(10);
		txtRules.setDisable(true); // read-only
		
		this.getStyleClass().add("vbox");
		this.getChildren().addAll(lblAverageAccuracy, lblBestAccuracy, txtRules);
		
		clear();
		updateTexts();
	}
	
	public void clear() {
		avgAccuracy = 0;
		bestAccuracy = 0;
		totalAccuracy = 0;
		numTestRuns = 0;
		bestRules = null;		
	}
	
	public void addResult(double accuracy, RuleSet rules) {
		avgAccuracy = numTestRuns * avgAccuracy + accuracy / (numTestRuns + 1);
		numTestRuns++;
		if (accuracy > bestAccuracy) {
			bestAccuracy = accuracy;
			bestRules = rules;
		}
		updateTexts();
	}
	
	public void updateTexts(Translator t) {
		this.t = t;
		updateTexts();
	}
	
	private void updateTexts() {
		if (t == null || numTestRuns == 0) {
			// Clear all displayed text
			lblAverageAccuracy.setText("");
			lblBestAccuracy.setText("");
			txtRules.setText("");
		} else {
			lblAverageAccuracy.setText(t.getString("program.results.averageAccuracy" + String.format("%,.2f", avgAccuracy)));
			lblBestAccuracy.setText(t.getString("program.results.bestAccuracy" + String.format("%,.2f", bestAccuracy)));
			
			StringBuffer buf = new StringBuffer();
			buf.append(t.getString("program.results.rules") + "\n");
			buf.append(bestRules.toString());
			txtRules.setText(buf.toString());
		}
	}
}
