package ilf.ui.view;

import ilf.ui.commonClasses.Translator;
import ilf.ui.view.StatusIndicator.Status;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;

public class PaneDataSelection extends PaneBase {
	public TextField txtTrainingData = new TextField();
	public Button btnTrainingData = new Button();
	
	private Label lblPartitioning = new Label();
	public ComboBox<String> cmbPartitionSize;
	
	public PaneDataSelection() {
		btnTrainingData.setId("file-browser");
		txtTrainingData.setId("file-name");
		
		ObservableList<String> options = FXCollections.observableArrayList();
		for (int i = 50; i <= 90; i += 10) {
			options.add(i + "/" + (100-i));
		}
		cmbPartitionSize = new ComboBox<>(options);
		cmbPartitionSize.getSelectionModel().select(4);
		
		Region spacer = new Region();
		this.addAll(txtTrainingData, btnTrainingData, spacer, lblPartitioning, cmbPartitionSize);
		HBox.setHgrow(spacer, Priority.ALWAYS);
		
		// Bind the "ready" property
        ready.bind(txtTrainingData.textProperty().isEmpty().not());
        
        // Our status is always OK, because this pane doesn't actually execute any process
        status.setStatus(Status.OK);
	}
	
	public void updateTexts(Translator t) {
		lblPaneTitle.setText(t.getString("program.data.title"));
		lblPartitioning.setText(t.getString("program.data.partitioning"));
	}
}
