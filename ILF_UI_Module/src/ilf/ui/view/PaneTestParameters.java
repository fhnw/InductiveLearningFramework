package ilf.ui.view;

import ilf.ui.commonClasses.Translator;
import ilf.ui.view.StatusIndicator.Status;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;

public class PaneTestParameters extends PaneBase {
	public ComboBox<Integer> cmbTestRuns;
	private Label lblOutput = new Label();
	public ComboBox<String> cmbOutput;
	
	public PaneTestParameters() {
		ObservableList<Integer> options = FXCollections.observableArrayList();
		options.add(1); options.add(2); options.add(5);
		options.add(10); options.add(20); options.add(50);
		options.add(100); options.add(200); options.add(500);
		cmbTestRuns = new ComboBox<>(options);
		cmbTestRuns.getSelectionModel().select(3);

		ObservableList<String> options2 = FXCollections.observableArrayList();
		options2.add("None"); 
		cmbOutput = new ComboBox<>(options2);
		cmbOutput.getSelectionModel().select(0);
		
		Region spacer = new Region();
		this.addAll(cmbTestRuns, spacer, lblOutput, cmbOutput);
		HBox.setHgrow(spacer, Priority.ALWAYS);
		
        // Our status is always OK, because this pane doesn't actually execute any process
        status.setStatus(Status.OK);
	}
	
	public void updateTexts(Translator t) {
		lblPaneTitle.setText(t.getString("program.testParameters.title"));
		lblOutput.setText(t.getString("program.testParameters.output"));
	}
}
