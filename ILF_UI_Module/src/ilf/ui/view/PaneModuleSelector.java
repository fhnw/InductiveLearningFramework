package ilf.ui.view;

import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;

public abstract class PaneModuleSelector extends PaneBase {
	public final ComboBox<String> cmbModuleSelector;
	
	public PaneModuleSelector() {
		super();
		cmbModuleSelector = new ComboBox<String>(getAvailableModules());
		cmbModuleSelector.getSelectionModel().select(0);
		
		Region spacer = new Region();
		this.addAll(cmbModuleSelector, spacer, status);
		HBox.setHgrow(spacer, Priority.ALWAYS);
	}

	@Override
	protected void disableControls(boolean disable) {
		cmbModuleSelector.setDisable(disable);
	}
	
	public String getSelectedModule() {
		return cmbModuleSelector.getSelectionModel().getSelectedItem();
	}
	
	/**
	 * This could be handled with reflection, but the code gets really ugly,
	 * and error-handling becomes difficult.
	 */
	protected abstract ObservableList<String> getAvailableModules();
}
