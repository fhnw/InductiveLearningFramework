package ilf.ui.model;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import ilf.data.DataSet;
import ilf.featureDeduction.ILF_featureDeduction;
import ilf.parser.ILF_parser;
import ilf.ui.ServiceLocator;
import ilf.ui.commonClasses.Translator;
import javafx.beans.property.SimpleStringProperty;
import javafx.concurrent.Task;

public class DataLoadingTask extends Task<Void> {
	ServiceLocator sl = ServiceLocator.getServiceLocator();
	Logger logger = sl.getLogger();
	Translator t = sl.getTranslator();

	private App_Model model;

	// Properties that can be monitored, to provide status updates to the user
	public final SimpleStringProperty parserStatus = new SimpleStringProperty("");
	public final SimpleStringProperty featureDeductionStatus = new SimpleStringProperty("");

	public DataLoadingTask(App_Model model) {
		this.model = model; // The models contains settings needed for execution
	}

	@Override
	protected Void call() throws Exception {
		logger.info("Data loading task: starting");
		// Load and execute parsing module
		List<ILF_parser> parserModules = ILF_parser.getInstances();
		ILF_parser parser = null;
		for (ILF_parser p : parserModules) {
			if (p.getName().equals(model.parserModule)) {
				parser = p;
				break;
			}
		}
		logger.info("Data loading task: parser '" + parser.getName() + "' loaded");

		DataSet ds = parser.getRecords(model.trainingDataFile);
		if (ds == null) {
			logger.info("Data loading task: data set parsed, null result");
			parserStatus.set(null);
			return null; // Abort - failure
		} else {
			int numRecords = ds.getDataRecords().size();
			logger.info("Data loading task: data set parsed, " + numRecords + " records found");
			parserStatus.set(Integer.toString(numRecords));
		}

		// Load and execute feature deduction module
		List<ILF_featureDeduction> fdModules = ILF_featureDeduction.getInstances();
		ILF_featureDeduction fdModule = null;
		for (ILF_featureDeduction fd : fdModules) {
			if (fd.getName().equals(model.featureDeductionModule)) {
				fdModule = fd;
				break;
			}
		}
		logger.info("Data loading task: feature deduction '" + fdModule.getName() + "' loaded");

		if (!fdModule.processDataSet(ds)) {
			featureDeductionStatus.set(null);
			logger.info("Data loading task: feature data-type deduction failed");
			return null; // Abort - failure
		} else {
			featureDeductionStatus.set(Integer.toString(ds.getFeatureDataTypes().length));
			logger.info("Data loading task: feature data-type deduction completed");
		}
		if (logger.getLevel().intValue() < Level.CONFIG.intValue()) {
			for (int i = 0; i < ds.getFeatureDataTypes().length; i++) {
				logger.config("Feature type for col " + i + " is " + ds.getFeatureDataTypes()[i].toString());
			}
		}		
		
		// If we are still here, we have succeeded - save the data set in the model
		model.setDataSet(ds);
		
		return null;
	}

}
