package ilf.ui.model;

import java.io.File;

import ilf.data.DataSet;
import ilf.ui.ServiceLocator;
import ilf.ui.abstractClasses.Model;

/**
 * Copyright 2018, FHNW, Prof. Dr. Brad Richards. All rights reserved. This code
 * is licensed under the terms of the BSD 3-Clause license (see the file
 * license.txt).
 * 
 * @author Brad Richards
 */
public class App_Model extends Model {
    ServiceLocator serviceLocator;
    File trainingDataFile = null;
    String parserModule = null;
    String featureDeductionModule = null;
    
    DataSet dataSet = null;
    
    public App_Model() {
        serviceLocator = ServiceLocator.getServiceLocator();        
        serviceLocator.getLogger().info("Application model initialized");
    }
    
    public File getTrainingDataFile() {
    	return trainingDataFile;
    }
    
    public void setTrainingDataFile(File trainingDataFile) {
        this.trainingDataFile = trainingDataFile;
    }
    
    public void setParserModule(String name) {
    	parserModule = name;
    }
    
    public void setFeatureDeductionModule(String name) {
    	featureDeductionModule = name;
    }

	public DataSet getDataSet() {
		return dataSet;
	}

	public void setDataSet(DataSet dataSet) {
		this.dataSet = dataSet;
	}
    
    
}
