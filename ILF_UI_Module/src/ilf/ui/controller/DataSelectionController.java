package ilf.ui.controller;

import java.io.File;

import ilf.ui.model.App_Model;
import ilf.ui.view.PaneDataSelection;
import javafx.scene.input.KeyEvent;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class DataSelectionController {
	public DataSelectionController(Stage stage, App_Model model, PaneDataSelection view) {
		// Open a file browser, when the button is clicked
		view.btnTrainingData.setOnAction(e -> {
			FileChooser fileChooser = new FileChooser();
			fileChooser.setTitle("Open Resource File");
			// fileChooser.getExtensionFilters().addAll(
			// new ExtensionFilter("All Files", "*.*"));
			File selectedFile = fileChooser.showOpenDialog(stage);
			if (selectedFile != null) {
				model.setTrainingDataFile(selectedFile);
				view.txtTrainingData.setText(selectedFile.toString());
			}
		});

		// Prohibit direct editing of the file name
		view.txtTrainingData.addEventFilter(KeyEvent.KEY_TYPED, e -> e.consume());
		
	}
}
