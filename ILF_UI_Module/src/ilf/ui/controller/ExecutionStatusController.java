package ilf.ui.controller;

import ilf.ui.ServiceLocator;
import ilf.ui.model.DataLoadingTask;
import ilf.ui.view.App_View;
import ilf.ui.view.StatusIndicator;

/**
 * Update the StatusLabels in the GUI, as the execution proceeds
 */
public class ExecutionStatusController {

	public ExecutionStatusController(App_View view, DataLoadingTask task) {
		ServiceLocator.getServiceLocator().getLogger().info("ExecutionStatusControlller created");

		task.parserStatus.addListener((o, oldValue, newValue) -> {
			if (newValue == null) {
				view.paneParsing.status.setStatus(StatusIndicator.Status.ERROR, newValue);
			} else {
				view.paneParsing.status.setStatus(StatusIndicator.Status.OK, newValue);
			}
		});
		
		task.featureDeductionStatus.addListener((o, oldValue, newValue) -> {
			if (newValue == null) {
				view.paneFeatureDeduction.status.setStatus(StatusIndicator.Status.ERROR, newValue);
			} else {
				view.paneFeatureDeduction.status.setStatus(StatusIndicator.Status.OK, newValue);
			}
		});
	}
}
