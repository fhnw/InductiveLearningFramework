package ilf.ui.controller;

import ilf.ui.model.App_Model;
import ilf.ui.view.PaneFeatureSelection;
import ilf.ui.view.featureSelection.FeatureSelectionDialog;
import javafx.scene.control.Dialog;
import javafx.stage.Stage;

public class FeatureSelectionController {
	public FeatureSelectionController(Stage stage, App_Model model, PaneFeatureSelection view) {
		// Open a file browser, when the button is clicked
		view.btnSelect.setOnAction(e -> {
			// Create and display custom dialog displaying all data columns
			Dialog<Void> featureSelectionDialog = new FeatureSelectionDialog(stage, model.getDataSet());	
			featureSelectionDialog.showAndWait();
			
			// After dialog is closed, read the selected purpose of each column
		});
	}

}
