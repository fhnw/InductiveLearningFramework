package ilf.ui.controller;

import ilf.ui.ServiceLocator;
import ilf.ui.abstractClasses.Controller;
import ilf.ui.model.App_Model;
import ilf.ui.model.DataLoadingTask;
import ilf.ui.view.App_View;
import ilf.ui.view.StatusIndicator.Status;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.control.Alert;

/**
 * Copyright 2018, FHNW, Prof. Dr. Brad Richards. All rights reserved. This code
 * is licensed under the terms of the BSD 3-Clause license (see the file
 * license.txt).
 * 
 * @author Brad Richards
 */
public class App_Controller extends Controller<App_Model, App_View> {
    ServiceLocator serviceLocator;

    public App_Controller(App_Model model, App_View view) {
        super(model, view);
        
        // register ourselves to listen for button clicks
        view.btnLoadData.setOnAction( this::btnLoadDataClick );
        view.btnTrain.setOnAction( this::btnTrainClick );

        // register ourselves to handle window-closing event
        view.getStage().setOnCloseRequest( e -> Platform.exit() );
        
        // Help-About dialog
        view.mnuHelpAbout.setOnAction( e -> {
        	Alert alert = new Alert(Alert.AlertType.INFORMATION, "Inductive Learning Framework\nVersion 0.0.1\nCopyright 2018, Brad Richards, FHNW");
        	alert.showAndWait();
        } );
        
        // Create further controllers, passing them the view components they work with
        new DataSelectionController(view.getStage(), model, view.paneDataSelection);
        new FeatureSelectionController(view.getStage(), model, view.paneFeatureSelection);
        
        // Set up enabling/disabling of controls, from top to bottom
        view.btnLoadData.disableProperty().bind(view.paneDataSelection.readyProperty().not());
        
        view.paneFeatureSelection.disableProperty().bind(view.paneFeatureDeduction.status.getStatusProperty().isNotEqualTo(Status.OK));
        view.paneAlgorithm.disableProperty().bind(view.paneFeatureDeduction.status.getStatusProperty().isNotEqualTo(Status.OK));
        view.btnTrain.disableProperty().bind(view.paneFeatureDeduction.status.getStatusProperty().isNotEqualTo(Status.OK));
        
        
        
        serviceLocator = ServiceLocator.getServiceLocator();        
        serviceLocator.getLogger().info("Application controller initialized");
    }
    
    /**
     * Create a task to execute the selected modules in order.
     */
    public void btnLoadDataClick(ActionEvent e) {
    	// Save user selections to the model
    	model.setParserModule(view.paneParsing.getSelectedModule());
    	model.setFeatureDeductionModule(view.paneFeatureDeduction.getSelectedModule());
    	
    	// Create and execute the task
    	DataLoadingTask t = new DataLoadingTask(model);
        new ExecutionStatusController(view, t);
    	new Thread(t).start();
    }
    
    /**
     * Create a task to train on the loaded data using the selected algorithm
     */
    public void btnTrainClick(ActionEvent e) {
    }
}
