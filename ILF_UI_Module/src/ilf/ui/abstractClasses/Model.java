package ilf.ui.abstractClasses;

/**
 * Copyright 2018, FHNW, Prof. Dr. Brad Richards. All rights reserved. This code
 * is licensed under the terms of the BSD 3-Clause license (see the file
 * license.txt).
 * 
 * @author Brad Richards
 */
public abstract class Model {
    protected Model() {
        // Nothing to see here, move along
    }
}
