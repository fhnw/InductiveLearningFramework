module ILF_UI_Module {
	// JavaFX must be able to start the application
	exports ilf.ui to javafx.graphics;

	// Java modules that we require
	requires java.logging;
	requires javafx.base;
	requires javafx.controls;
	requires javafx.graphics;
	
	// Other project modules
	requires ILF_Data_Module;
	requires ILF_Parser_Interface;
	requires ILF_Feature_Deduction_Interface;
	requires ILF_Algorithm_Interface;
	requires ILF_Rule_Module;
}