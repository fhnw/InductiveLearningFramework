The CSV file format contains one data record per line. All data is read in as
strings, which may then be further processed by the Feature-Deduction module.

Fields within a record must be separated by commas. In a future version,
this requirement may be made more flexible.

One header row is required, to give names to the columns.

A second header row is optional. If present, it identifies which columns should
be used as inputs into the learning process, which ones should be the outputs,
and which ones should be ignored. If present, and preserved through Feature
Deduction, these become the default (pre-selected) values for feature selection.

This second header row is identified automatically, by inspecting the value in
the first column. If this is one of the values "Input", "Output", or "Ignore",
then the second row is presumed to be this header. If any other value is present,
the second row is presumed to be the first data row.  