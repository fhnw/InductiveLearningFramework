module ILF_Parser_CSV {
	// Needs classes from this module
	requires ILF_Parser_Interface;
	requires ILF_Data_Module;
	// requires ILF_Data_Module; // provided transitively through ILF_Parser_Interface
	
	// Provides an implementation of this interface in the named class
	// Each module can only provide each service once! 
	// Alternate implementations must be in different modules!
	provides ilf.parser.ILF_parser with ilf.parser.csv.ILF_parser_CSV;
}