package ilf.parser.csv;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import ilf.data.DataSet;
import ilf.data.FeaturePurpose;
import ilf.data.FeatureType;
import ilf.parser.ILF_parser;

public class ILF_parser_CSV implements ILF_parser {
	private int numFields;
	private String splitChar = ",";

	@Override
	public String getName() {
		return "CSV";
	}

	@Override
	public DataSet getRecords(File fileIn) throws IOException {
		DataSet data = null;
		try ( BufferedReader in = new BufferedReader(new FileReader(fileIn)) ) {
			String[] featureNames = in.readLine().split(splitChar);
			numFields = featureNames.length;
			if (numFields < 1) throw new IOException("Zero fields found");
			data = new DataSet(featureNames);
	
			int rowNum = 2;
			String nextLine;
			while ((nextLine = in.readLine()) != null) {
				String[] values = nextLine.split(splitChar);
				if (values.length != numFields) throw new IOException("Incorrect number of fields on row" + rowNum);
				
				boolean isFeaturePurposeRow = false;
				if (rowNum == 2) {
					// Check if this is a featurePurpose row
					for (FeaturePurpose fp : FeaturePurpose.values()) {
						isFeaturePurposeRow |= fp.toString().equalsIgnoreCase(values[0]);
					}
				}
				
				if (isFeaturePurposeRow) {
					data.setFeaturePurposes(parseFeaturePurposes(values));
				} else { // normal data record
					data.addRecord(values);
				}
				rowNum++;
			}		
		}
		
		// Add feature types - all Strings at this point
		FeatureType[] featureTypes = new FeatureType[numFields]; 
		for (int i = 0; i < numFields; i++) {
			featureTypes[i] = new FeatureType(String.class);
		}
		data.setFeatureDataTypes(featureTypes);
		
		return data;
	}

	private FeaturePurpose[] parseFeaturePurposes(String[] values) throws IOException {
		FeaturePurpose[] purposes = new FeaturePurpose[values.length];
		
		for (int i = 0; i < values.length; i++) {
			for (FeaturePurpose fp : FeaturePurpose.values()) {
				if (fp.toString().equalsIgnoreCase(values[i])) {
					purposes[i] = fp;
					break;
				}
			}
			if (purposes[i] == null) throw new IOException("Feature purpose in column " + i + " is not recognized");
		}
		return purposes;
	}
}
