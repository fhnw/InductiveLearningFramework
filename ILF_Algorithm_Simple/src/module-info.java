module ILF_Algorithm_Simple {
	// Needs classes from this module
	requires ILF_Algorithm_Interface;
	requires ILF_Data_Module; // actually provided transitively through ILF_Algorithm_Interface
	requires ILF_Rule_Module; // actually provided transitively through  ILF_Algorithm_Interface
	
	// Provides an implementation of this interface in the named class
	// Each module can only provide each service once! 
	// Alternate implementations must be in different modules!
	provides ilf.algorithm.ILF_algorithm with ilf.algorithm.simple.ILF_algorithm_Simple;
}