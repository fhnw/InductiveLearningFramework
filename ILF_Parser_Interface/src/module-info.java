module ILF_Parser_Interface {
	exports ilf.parser;
	
	// Requires data representation classes
	requires transitive ILF_Data_Module;
	
	// Required for static getInstances method
	uses ilf.parser.ILF_parser;
}