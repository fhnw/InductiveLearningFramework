package ilf.parser;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.ServiceLoader;

import ilf.data.DataSet;

public interface ILF_parser {
	// The interface required for parsers
	public String getName(); // Each implementation has a name
	public DataSet getRecords(File filein) throws IOException; // Read from a file, provide a data set

	/**
	 * The static method used to discover available implementations on the module
	 * path
	 */
	static List<ILF_parser> getInstances() {
		ServiceLoader<ILF_parser> services = ServiceLoader.load(ILF_parser.class);
		List<ILF_parser> list = new ArrayList<>();
		services.iterator().forEachRemaining(list::add);
		return list;
	}
}
